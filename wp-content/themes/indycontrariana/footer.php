<div id="footer">
        <div id="colophon">
 
            <div id="site-info">
				
            </div><!-- #site-info -->
 
        </div><!-- #colophon -->
    </div><!-- #footer -->

<?php wp_footer(); ?>
<!-- Placed at the end of the document so the pages load faster -->

    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo get_bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_bloginfo('template_url'); ?>/js/less.js" type="text/javascript"></script>
    <script src="<?php echo get_bloginfo('template_url'); ?>/js/custom.js" type="text/javascript"></script>
    
</body>
</html>