<?php /* Template Name: Book */
get_header(); ?>

<style>
	
	body {
		
		background: #ffffff;
		
	}
	
</style>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		
		<img class="book-image" src="/wp-content/themes/logger/images/book.png" />
		
	</div>
	
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		
		<h1 class="book-header">Contrary to Popular Belief</h1>
		
		<h2 class="book-sub-header">A Chronicle of a Progressive in Indiana</h2>
		
		<p class="book-description">In the spring of 2014, Leppert started writing a blog.  It was the usual kind of blog being written for the usual kinds of reasons. But a funny thing happened when he took a couple of risks with his writing:  he got away with it.  And then he took his gloves off for good. Within a year, his “blog” was elevated to a “column” and began showing up in mainstream publications throughout Indiana.  After publishing more than a hundred installments, a group of recurring political and cultural themes began to emerge. From RFRA and guns, to the economy and parenting, Leppert takes a provocative view on all of it.  Contrary To Popular Belief gives the real reasons why and from where his opinions are based. It is a chronicle being released purposely in summer of 2016 for readers to use as a guide during a historically chaotic political season.<br><br>Listen to Michael Leppert's latest on Indiana's public media, WFIU: <a href="http://indianapublicmedia.org/profiles/lobbyist-michael-leppert/">http://indianapublicmedia.org/profiles/lobbyist-michael-leppert/</a><br><br>Read what Nuvo has to say about Michael Leppert's new book: <a href="http://www.nuvo.net/indianapolis/michael-lepperts-new-book-breaks-down-being-a-progressive-in-indiana/Content?oid=4273002">http://www.nuvo.net/indianapolis/michael-lepperts-new-book-breaks-down-being-a-progressive-in-indiana/Content?oid=4273002</a></p>
		
<!-- 		<a class="book-button" href="">read an excerpt</a> -->
		
		<a class="book-button" href="http://contrariana.com/store/">buy now</a>
		
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<h2 class="book-sub-header underline">as seen in</h2>
		
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 article-buttons">
			
			<a id="myBtn">
			
				<img class="book-image" src="/wp-content/themes/logger/images/statehouse.png" />
				
			</a>
			
		</div>
		
		<div id="myModal" class="modal">

			<div class="modal-content">
				
				<span class="close">x</span>
				
				<br>
				
				<br>
				
				<iframe src="http://docs.google.com/gview?url=http://contrariana.com/wp-content/themes/logger/images/11-19-15 Page 12.pdf&embedded=true" style="width:100%; height:700px;" frameborder="0"></iframe>
				
			</div>
		
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 article-buttons">
			
			<a id="myBtn_2">
			
				<img class="book-image" src="/wp-content/themes/logger/images/NUVO_logo.png" />
				
			</a>
			
		</div>
		
		<div id="myModal_2" class="modal">

			<div class="modal-content">
				
				<span class="close_2">x</span>
				
				<br>
				
				<br>
				
				<iframe src="http://docs.google.com/gview?url=http://contrariana.com/wp-content/themes/logger/images/021016_NUVO_PG004-1.pdf&embedded=true" style="width:100%; height:700px;" frameborder="0"></iframe>
				
			</div>
		
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 article-buttons">
			
				<img class="book-image" src="/wp-content/themes/logger/images/IBJ_logo.png" />
			
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 article-buttons">
			
			<a id="myBtn_1">
			
				<img class="book-image" src="/wp-content/themes/logger/images/the_republic_logo.png" />
				
			</a>
			
		</div>
		
		<div id="myModal_1" class="modal">

			<div class="modal-content">
				
				<span class="close_1">x</span>
				
				<br>
				
				<br>
				
				<img class="book-image" src="/wp-content/themes/logger/images/The_Republic_Sun__Jul_12__2015_.jpg" />
				
			</div>
		
		</div>
		
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 article-buttons">
			
				<img class="book-image" src="/wp-content/themes/logger/images/indy_star_logo.png" />
			
		</div>
		
	</div>
	
	<p class="click-them">[ click logo to read articles ]</p>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<h2 class="book-sub-header underline">testimonials</h2>
		
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 testimonial">

			<p class="test-description">“More than once I’ve read Mike Leppert’s column and thought, “I wish I’d written that.” When that happens I’m both thrilled and a little disappointed. Thrilled because Leppert’s turned the subject and made me think differently. Disappointed because I’ve written newspaper columns for a long time and Leppert’s a lobbyist. It’s not fair that he’s so good at lobbying and column writing.<br><br>

The disappointment’s short-lived, though, because Leppert’s doing something I wish more writers did—he’s made me think. Too often we’ll let the “conventional wisdom” determine our viewpoint, especially in things politic, and all that means is we’ve given up thinking critically about the day’s issues. Leppert’s writing shakes us all up from time to time, and I’m OK with that.”<br><br>

 

--John Ketzenberger<br>

President of the Indiana Fiscal Policy Institute<br>

Columnist for the Indianapolis Star<br>

May 2016</p>

		<p class="test-description">“Mike Leppert describes himself as an ‘opinionated smart ass.’ He’s wrong. He’s a knowledgeable opinionated smart ass — and that makes all the difference. Mike knows state government inside out, having worked as executive director of the Indiana Utility Regulatory Commission before joining the ranks of the lobbyists. He knows the players in the legislature, the executive branch and the media and isn’t afraid of offending any of them. As a former member of the Statehouse press corps myself, I loved it.”<br><br>

 

--Mary Beth Schneider, former statehouse reporter, Indianapolis Star</p>
			
		</div>
		
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 testimonial">
			
			<p class="test-description">“Mike Leppert’s columns are about far more than just good writing and snark, although that certainly makes them entertaining to read. Instead, he provides a unique perspective as a lobbyist who knows the people making key policy decisions and yet remains unafraid to question the status quo and challenge those leaders to do better for Hoosiers. Still, Mr. Leppert is at his best when he writes not as an insider but as a citizen simply trying to learn more about his community, its institutions and its people and sharing his stories along the way.”<br><br>

 

--Lesley Weidenbener, Editor, Indianapolis Business Journal</p>

			<p class="test-description">“The Contrarian (Mike Leppert) has the balls to write honestly about the toughest issues. You may not agree with him but you will at least engage in the discussion.”<br><br>

 

--Joel Miller, Marion County Democrat Chairman</p>

			<p class="test-description">“On any given day spent in the vapid halls of the statehouse, Leppert will approach me with a piercing grin and I know three things are about to happen: 1) I’m about to get a razor sharp perspective on the issue of the day 2) I’m going to think about something differently, and 3) I’m going to laugh out loud in the process. Contrary to Popular Belief reflects Leppert’s thoughtfulness and wit on matters of policy and politics; a perspective that is driven by reason, pragmatism, and the idea that political discourse must be more than a memorized list of partisan soundbites to complicated questions.”<br><br>

 

Mike O’Brien, Hendricks County Republican Chairman, lobbyist, Barnes & Thornburg, LLP</p>
			
		</div>
		
	</div>
	
</div>

<?php get_footer(); ?>