<?php /* Template Name: Event */
get_header(); ?>

<style>
	
	body {
		
		background: #ffffff;
		
	}

</style>
<?php the_post();?>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );?>
		<img src="<?php echo $large_image_url[0];?>" class="img-responsive"/>
		
	</div>
	
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<h1 class="book-header"><?php echo get_the_title();?></h1>
		
		<?php the_content();?>
		
<!-- 		<a class="book-button" href="">read an excerpt</a> -->
		
		<a class="book-button" href="/store/#!/Scars-on-45-Event/p/70908835/category=0">reserve a spot</a>
		
	</div>


</div>
</div>
<br/>
&nbsp;
<hr style="border-bottom: 1px solid #00305b"/>
<br />
	&nbsp;
	<br/>
	&nbsp;
	<br/>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="col-lg-4 col-md-4 col-sm-10 col-xs-10 col-md-offset-0 col-sm-offset-2 col-xs-offset-2">
			<img src="../wp-content/uploads/2016/09/pic3.png" class="img-responsive gallery-img-custom"/>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-10 col-xs-10 col-md-offset-0 col-sm-offset-2 col-xs-offset-2">
			<img src="../wp-content/uploads/2016/09/pic2.png" class="img-responsive gallery-img-custom"/>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-10 col-xs-10 col-md-offset-0 col-sm-offset-2 col-xs-offset-2">
			<img src="../wp-content/uploads/2016/09/pic1.png" class="img-responsive gallery-img-custom"/>
		</div>

	</div>
</div>

<?php get_footer(); ?>