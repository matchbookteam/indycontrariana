<?php
wp_reset_query();
if (is_home()) {
    $home_page_sidebar = vpanel_options("sidebar_home");
    if ($home_page_sidebar == "default") {
        dynamic_sidebar('sidebar_default');
    }else {
        dynamic_sidebar(sanitize_title($home_page_sidebar));
    }
}else if (is_author()) {
	$author_sidebar = vpanel_options("author_sidebar");
	$author_sidebar_layout = vpanel_options("author_sidebar_layout");
	if ($author_sidebar_layout == "default" || $author_sidebar == "default") {
	    dynamic_sidebar('sidebar_default');
	}else {
	    dynamic_sidebar(sanitize_title($author_sidebar));
	}
}else if (is_category()) {
	$category_id = get_query_var('cat');
	$categories = get_option("categories_$category_id");
	$cat_sidebar_layout = (isset($categories["cat_sidebar_layout"])?$categories["cat_sidebar_layout"]:"default");
	$cat_sidebar = (isset($categories["cat_sidebar"])?$categories["cat_sidebar"]:"default");
	if ($cat_sidebar_layout == "default" || $cat_sidebar == "default") {
	    dynamic_sidebar('sidebar_default');
	}else {
	    dynamic_sidebar(sanitize_title($cat_sidebar));
	}
}else if(is_single() or is_page()) {
    $vbegy_what_sidebar = rwmb_meta('vbegy_what_sidebar','select',$post->ID);
    $sidebar_post = rwmb_meta('vbegy_sidebar','radio',$post->ID);
    if ($sidebar_post != "centered" && $sidebar_post != "full") {
	    if (isset($vbegy_what_sidebar) && $vbegy_what_sidebar != "default" && $vbegy_what_sidebar != "") {
		    dynamic_sidebar(sanitize_title($vbegy_what_sidebar));
	    }else {
	        dynamic_sidebar('sidebar_default');
	    }
    }
}else  {
    $else_sidebar = vpanel_options("else_sidebar");
    if ($else_sidebar == "default") {
        dynamic_sidebar('sidebar_default');
    }else {
        dynamic_sidebar(sanitize_title($else_sidebar));
    }
}
?>