<?php if (is_single() || is_page()) {
						$vbegy_content_adv_type = rwmb_meta('vbegy_content_adv_type','radio',$post->ID);
						$vbegy_content_adv_code = rwmb_meta('vbegy_content_adv_code','textarea',$post->ID);
						$vbegy_content_adv_href = rwmb_meta('vbegy_content_adv_href','text',$post->ID);
						$vbegy_content_adv_img = rwmb_meta('vbegy_content_adv_img','upload',$post->ID);
					}
					
					if ((is_single() || is_page()) && (($vbegy_content_adv_type == "display_code" && $vbegy_content_adv_code != "") || ($vbegy_content_adv_type == "custom_image" && $vbegy_content_adv_img != ""))) {
						$content_adv_type = $vbegy_content_adv_type;
						$content_adv_code = $vbegy_content_adv_code;
						$content_adv_href = $vbegy_content_adv_href;
						$content_adv_img = $vbegy_content_adv_img;
					}else {
						$content_adv_type = vpanel_options("content_adv_type");
						$content_adv_code = vpanel_options("content_adv_code");
						$content_adv_href = vpanel_options("content_adv_href");
						$content_adv_img = vpanel_options("content_adv_img");
					}
					if (($content_adv_type == "display_code" && $content_adv_code != "") || ($content_adv_type == "custom_image" && $content_adv_img != "")) {
						echo '<div class="clearfix"></div>
						<div class="advertising">';
						if ($content_adv_type == "display_code") {
							echo stripcslashes($content_adv_code);
						}else {
							if ($content_adv_href != "") {
								echo '<a href="'.$content_adv_href.'">';
							}
							echo '<img alt="" src="'.$content_adv_img.'">';
							if ($content_adv_href != "") {
								echo '</a>';
							}
						}
						echo '</div><!-- End advertising -->
						<div class="clearfix"></div>';
					}?>
				</div><!-- End main-content -->
				<div class="col-md-4 sidebar">
					<?php get_sidebar();?>
				</div><!-- End sidebar -->
			</div><!-- End row -->
		</div><!-- End container -->
	</div><!-- End sections -->
	
	<div class="clearfix"></div>
	
	<?php
	wp_reset_query();
	$vbegy_custom_header = rwmb_meta('vbegy_custom_header','checkbox',$post->ID);
	if ((is_single() || is_page()) && isset($vbegy_custom_header) && $vbegy_custom_header == 1) {
		$vbegy_header_style = rwmb_meta('vbegy_header_style','select',$post->ID);
		$vbegy_header_menu = rwmb_meta('vbegy_header_menu','checkbox',$post->ID);
		$vbegy_header_menu_style = rwmb_meta('vbegy_header_menu_style','radio',$post->ID);
		$vbegy_header_fixed = rwmb_meta('vbegy_header_fixed','checkbox',$post->ID);
		$vbegy_logo_display = rwmb_meta('vbegy_logo_display','radio',$post->ID);
		$vbegy_logo_img = rwmb_meta('vbegy_logo_img','upload',$post->ID);
		$vbegy_header_search = rwmb_meta('vbegy_header_search','checkbox',$post->ID);
		$vbegy_header_follow = rwmb_meta('vbegy_header_follow','checkbox',$post->ID);
		$vbegy_header_follow_style = rwmb_meta('vbegy_header_follow_style','radio',$post->ID);
		$vbegy_facebook_icon_h = rwmb_meta('vbegy_facebook_icon_h','text',$post->ID);
		$vbegy_twitter_icon_h = rwmb_meta('vbegy_twitter_icon_h','text',$post->ID);
		$vbegy_gplus_icon_h = rwmb_meta('vbegy_gplus_icon_h','text',$post->ID);
		$vbegy_linkedin_icon_h = rwmb_meta('vbegy_linkedin_icon_h','text',$post->ID);
		$vbegy_dribbble_icon_h = rwmb_meta('vbegy_dribbble_icon_h','text',$post->ID);
		$vbegy_youtube_icon_h = rwmb_meta('vbegy_youtube_icon_h','text',$post->ID);
		$vbegy_vimeo_icon_h = rwmb_meta('vbegy_vimeo_icon_h','text',$post->ID);
		$vbegy_skype_icon_h = rwmb_meta('vbegy_skype_icon_h','text',$post->ID);
		$vbegy_flickr_icon_h = rwmb_meta('vbegy_flickr_icon_h','text',$post->ID);
		$vbegy_soundcloud_icon_h = rwmb_meta('vbegy_soundcloud_icon_h','text',$post->ID);
		$vbegy_instagram_icon_h = rwmb_meta('vbegy_instagram_icon_h','text',$post->ID);
		$vbegy_pinterest_icon_h = rwmb_meta('vbegy_pinterest_icon_h','text',$post->ID);
		$header_style = $vbegy_header_style;
		$header_menu = $vbegy_header_menu;
		if ($vbegy_header_menu == 1) {
			$header_menu = "on";
		}
		$header_menu_style = $vbegy_header_menu_style;
		$header_fixed = $vbegy_header_fixed;
		if ($vbegy_header_fixed == 1) {
			$header_fixed = "on";
		}
		$logo_display = $vbegy_logo_display;
		$logo_img = $vbegy_logo_img;
		$header_search = $vbegy_header_search;
		if ($vbegy_header_search == 1) {
			$header_search = "on";
		}
		$header_follow = $vbegy_header_follow;
		if ($vbegy_header_follow == 1) {
			$header_follow = "on";
		}
		$header_follow_style = $vbegy_header_follow_style;
		$facebook_icon_h = $vbegy_facebook_icon_h;
		$twitter_icon_h = $vbegy_twitter_icon_h;
		$gplus_icon_h = $vbegy_gplus_icon_h;
		$linkedin_icon_h = $vbegy_linkedin_icon_h;
		$dribbble_icon_h = $vbegy_dribbble_icon_h;
		$youtube_icon_h = $vbegy_youtube_icon_h;
		$vimeo_icon_h = $vbegy_vimeo_icon_h;
		$skype_icon_h = $vbegy_skype_icon_h;
		$flickr_icon_h = $vbegy_flickr_icon_h;
		$soundcloud_icon_h = $vbegy_soundcloud_icon_h;
		$instagram_icon_h = $vbegy_instagram_icon_h;
		$pinterest_icon_h = $vbegy_pinterest_icon_h;
		$head_slide = rwmb_meta('vbegy_head_slide','select',$post->ID);
		$head_slide_style = rwmb_meta('vbegy_head_slide_style','select',$post->ID);
		$slide_overlay = rwmb_meta('vbegy_slide_overlay','select',$post->ID);
		$orderby_slide = rwmb_meta('vbegy_orderby_slide','select',$post->ID);
		$excerpt_title_slideshow = rwmb_meta('vbegy_excerpt_title_slideshow','text',$post->ID);
		$excerpt_slideshow = rwmb_meta('vbegy_excerpt_slideshow','text',$post->ID);
		$slideshow_number = rwmb_meta('vbegy_slideshow_number','text',$post->ID);
		$slideshow_display = rwmb_meta('vbegy_slideshow_display','select',$post->ID);
		$slideshow_single_category = rwmb_meta('vbegy_slideshow_single_category','select',$post->ID);
		$slideshow_categories = rwmb_meta('vbegy_slideshow_categories','type=checkbox_list',$post->ID);
		$slideshow_posts = get_post_meta($post->ID,'vbegy_slideshow_posts');
		$excerpt_title_thumbnail = rwmb_meta('vbegy_excerpt_title_thumbnail','text',$post->ID);
		$thumbnail_number = rwmb_meta('vbegy_thumbnail_number','text',$post->ID);
		$orderby_thumbnail = rwmb_meta('vbegy_orderby_thumbnail','select',$post->ID);
		$thumbnail_display = rwmb_meta('vbegy_thumbnail_display','select',$post->ID);
		$thumbnail_single_category = rwmb_meta('vbegy_thumbnail_single_category','select',$post->ID);
		$thumbnail_categories = rwmb_meta('vbegy_thumbnail_categories','type=checkbox_list',$post->ID);
		$thumbnail_posts = get_post_meta($post->ID,'vbegy_thumbnail_posts');
		$head_slide_background = rwmb_meta('vbegy_head_slide_background','select',$post->ID);
		$head_slide_background_color = rwmb_meta('vbegy_head_slide_background_color','color',$post->ID);
		$head_slide_background_img = rwmb_meta('vbegy_head_slide_background_img','upload',$post->ID);
		$head_slide_background_repeat = rwmb_meta('vbegy_head_slide_background_repeat','select',$post->ID);
		$head_slide_background_fixed = rwmb_meta('vbegy_head_slide_background_fixed','select',$post->ID);
		$head_slide_background_position_x = rwmb_meta('vbegy_head_slide_background_position_x','select',$post->ID);
		$head_slide_background_position_y = rwmb_meta('vbegy_head_slide_background_position_y','select',$post->ID);
		$head_slide_background_position = $head_slide_background_position_x." ".$head_slide_background_position_y;
		$head_slide_full_screen_background = rwmb_meta('vbegy_head_slide_background_full','checkbox',$post->ID);
		if ($head_slide_full_screen_background == 1) {
			$head_slide_full_screen_background = "on";
		}
		$head_slide_custom_background = "on";
		$news_ticker = rwmb_meta('vbegy_news_ticker','checkbox',$post->ID);
		if ($news_ticker == 1) {
			$news_ticker = "on";
		}
		$news_excerpt_title = rwmb_meta('vbegy_news_excerpt_title','text',$post->ID);
		$news_number = rwmb_meta('vbegy_news_number','text',$post->ID);
		$news_categories_s = rwmb_meta('vbegy_news_categories','type=checkbox_list',$post->ID);
		$video_head = rwmb_meta('vbegy_video_head','select',$post->ID);
		$video_id_head = rwmb_meta('vbegy_video_id_head','text',$post->ID);
		$custom_embed_head = rwmb_meta('vbegy_custom_embed_head','textarea',$post->ID);
	}else {
		$header_style = vpanel_options("header_style");
		$header_menu = vpanel_options("header_menu");
		$header_menu_style = vpanel_options("header_menu_style");
		$header_fixed = vpanel_options("header_fixed");
		$logo_display = vpanel_options("logo_display");
		$logo_img = vpanel_options("logo_img");
		$header_search = vpanel_options("header_search");
		$header_follow = vpanel_options("header_follow");
		$header_follow_style = vpanel_options("header_follow_style");
		$facebook_icon_h = vpanel_options("facebook_icon_h");
		$twitter_icon_h = vpanel_options("twitter_icon_h");
		$gplus_icon_h = vpanel_options("gplus_icon_h");
		$linkedin_icon_h = vpanel_options("linkedin_icon_h");
		$dribbble_icon_h = vpanel_options("dribbble_icon_h");
		$youtube_icon_h = vpanel_options("youtube_icon_h");
		$skype_icon_h = vpanel_options("skype_icon_h");
		$vimeo_icon_h = vpanel_options("vimeo_icon_h");
		$flickr_icon_h = vpanel_options("flickr_icon_h");
		$instagram_icon_h = vpanel_options("instagram_icon_h");
		$soundcloud_icon_h = vpanel_options("soundcloud_icon_h");
		$pinterest_icon_h = vpanel_options("pinterest_icon_h");
		$head_slide = vpanel_options('head_slide');
		$head_slide_style = vpanel_options("head_slide_style");
		$slide_overlay = vpanel_options("slide_overlay");
		$orderby_slide = vpanel_options("orderby_slide");
		$excerpt_title_slideshow = vpanel_options("excerpt_title_slideshow");
		$excerpt_slideshow = vpanel_options("excerpt_slideshow");
		$slideshow_number = vpanel_options("slideshow_number");
		$slideshow_display = vpanel_options("slideshow_display");
		$slideshow_single_category = vpanel_options("slideshow_single_category");
		$slideshow_categories = vpanel_options("slideshow_categories");
		$slideshow_posts = vpanel_options("slideshow_posts");
		$excerpt_title_thumbnail = vpanel_options("excerpt_title_thumbnail");
		$thumbnail_number = vpanel_options("thumbnail_number");
		$orderby_thumbnail = vpanel_options("orderby_thumbnail");
		$thumbnail_display = vpanel_options("thumbnail_display");
		$thumbnail_single_category = vpanel_options("thumbnail_single_category");
		$thumbnail_categories = vpanel_options("thumbnail_categories");
		$thumbnail_posts = vpanel_options("thumbnail_posts");
		$head_slide_background = vpanel_options('head_slide_background');
		$head_slide_custom_background = vpanel_options('head_slide_custom_background');
		$head_slide_background_color = $head_slide_custom_background["color"];
		$head_slide_background_img = $head_slide_custom_background["image"];
		$head_slide_background_repeat = $head_slide_custom_background["repeat"];
		$head_slide_background_position = $head_slide_custom_background["position"];
		$head_slide_background_fixed = $head_slide_custom_background["attachment"];
		$head_slide_full_screen_background = vpanel_options('head_slide_full_screen_background');
		$news_ticker = vpanel_options('news_ticker');
		$news_excerpt_title = vpanel_options('news_excerpt_title');
		$news_number = vpanel_options('news_number');
		$slideshow_display = vpanel_options('slideshow_display');
		$news_categories = vpanel_options('news_categories');
		$slideshow_posts = vpanel_options('slideshow_posts');
		$video_head = vpanel_options('video_head');
		$video_id_head = vpanel_options('video_id_head');
		$custom_embed_head = vpanel_options('custom_embed_head');
	}
	if ($head_slide == "footer") {
		include(locate_template("includes/head_slide.php"));
	}
	
	$footer_top = vpanel_options("footer_top");
	if ($footer_top == "on") {
		$subscribe_f = vpanel_options("subscribe_f");
		$footer_layout_1 = vpanel_options("footer_layout_1");
		$footer_layout_2 = vpanel_options("footer_layout_2");?>
		<footer id="footer-top">
			<div class="container">
				<div class="row">
					<?php if ($subscribe_f == "on") {
						$feedburner_h3 = vpanel_options("feedburner_h3");
						$feedburner_p = vpanel_options("feedburner_p");
						$feedburner_id = vpanel_options("feedburner_id");
						?>
						<div class="col-md-12">
							<div class="footer-subscribe">
								<div class="row">
									<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo esc_attr($feedburner_id); ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
										<div class="col-md-8">
											<?php if ($feedburner_h3 != "") {?>
												<h3><?php echo esc_attr($feedburner_h3)?></h3>
											<?php }
											if ($feedburner_p != "") {?>
												<p><?php echo esc_attr($feedburner_p)?></p>
											<?php }?>
										</div>
										<div class="col-md-4">
										    <input name="email" type="text" value="<?php _e("Email","vbegy");?>" onfocus="if(this.value=='<?php _e("Email","vbegy");?>')this.value='';" onblur="if(this.value=='')this.value='<?php _e("Email","vbegy");?>';">
											<input type="hidden" value="<?php echo esc_attr($feedburner_id); ?>" name="uri">
											<input type="hidden" name="loc" value="en_US">	
											<input name="submit" type="submit" id="submit" class="button color small sidebar_submit" value="<?php _e('Subscribe','vbegy')?>">
										</div>
									</form>
								</div>
							</div>
						</div>
					<?php }
					if ($footer_layout_1 != "footer_no") {
						if ($footer_layout_1 == "footer_1c") {?>
							<div class="col-md-12">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_2c") {?>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_3c") {?>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_3c_sidebar_1');?>
							</div>
						<?php }else if ($footer_layout_1 == "footer_4c") {?>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_1c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_2c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_3c_sidebar_1');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_4c_sidebar_1');?>
							</div>
						<?php }
					}?>
					<div class="clearfix"></div>
					<?php if ($footer_layout_2 != "footer_no") {
						if ($footer_layout_2 == "footer_1c") {?>
							<div class="col-md-12">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_2c") {?>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-6">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_3c") {?>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
							<div class="col-md-4">
								<?php dynamic_sidebar('footer_3c_sidebar_2');?>
							</div>
						<?php }else if ($footer_layout_2 == "footer_4c") {?>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_1c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_2c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_3c_sidebar_2');?>
							</div>
							<div class="col-md-3">
								<?php dynamic_sidebar('footer_4c_sidebar_2');?>
							</div>
						<?php }
					}?>
				</div><!-- End row -->
			</div><!-- End container -->
		</footer>
	<?php }?>
	
	<footer id="footer"<?php echo ($footer_top == "on"?"class='footer-no-top'":"")?>>
		<div class="container">
			<div class="copyrights"><?php echo vpanel_options("footer_copyrights")?></div>
			<div class="social-ul">
				<ul>
					<?php
					$social_icon_f = vpanel_options("social_icon_f");
					if ($social_icon_f == "on") {
						$facebook_icon_f = vpanel_options("facebook_icon_f");
						$twitter_icon_f = vpanel_options("twitter_icon_f");
						$gplus_icon_f = vpanel_options("gplus_icon_f");
						$linkedin_icon_f = vpanel_options("linkedin_icon_f");
						$dribbble_icon_f = vpanel_options("dribbble_icon_f");
						$youtube_icon_f = vpanel_options("youtube_icon_f");
						$skype_icon_f = vpanel_options("skype_icon_f");
						$vimeo_icon_f = vpanel_options("vimeo_icon_f");
						$flickr_icon_f = vpanel_options("flickr_icon_f");
						$soundcloud_icon_f = vpanel_options("soundcloud_icon_f");
						$instagram_icon_f = vpanel_options("instagram_icon_f");
						$pinterest_icon_f = vpanel_options("pinterest_icon_f");
						$rss_icon_f = vpanel_options("rss_icon_f");
						$rss_icon_f_other = vpanel_options("rss_icon_f_other");
						if ($facebook_icon_f) {?>
							<li class="social-facebook"><a href="<?php echo esc_url($facebook_icon_f)?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php }
						if ($twitter_icon_f) {?>
							<li class="social-twitter"><a href="<?php echo esc_url($twitter_icon_f)?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php }
						if ($gplus_icon_f) {?>
							<li class="social-google"><a href="<?php echo esc_url($gplus_icon_f)?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php }
						if ($linkedin_icon_f) {?>
							<li class="social-linkedin"><a href="<?php echo esc_url($linkedin_icon_f)?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php }
						if ($dribbble_icon_f) {?>
							<li class="social-dribbble"><a href="<?php echo esc_url($dribbble_icon_f)?>" target="_blank"><i class="fa fa-dribbble"></i></a></li>
						<?php }
						if ($youtube_icon_f) {?>
							<li class="social-youtube"><a href="<?php echo esc_url($youtube_icon_f)?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
						<?php }
						if ($vimeo_icon_f) {?>
							<li class="social-vimeo"><a href="<?php echo esc_url($vimeo_icon_f)?>" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>
						<?php }
						if ($skype_icon_f) {?>
							<li class="social-skype"><a href="<?php echo esc_url($skype_icon_f)?>" target="_blank"><i class="fa fa-skype"></i></a></li>
						<?php }
						if ($flickr_icon_f) {?>
							<li class="social-flickr"><a href="<?php echo esc_url($flickr_icon_f)?>" target="_blank"><i class="fa fa-flickr"></i></a></li>
						<?php }
						if ($soundcloud_icon_f) {?>
							<li class="social-soundcloud"><a href="<?php echo esc_url($soundcloud_icon_f)?>" target="_blank"><i class="fa fa-soundcloud"></i></a></li>
						<?php }
						if ($instagram_icon_f) {?>
							<li class="social-instagram"><a href="<?php echo esc_url($instagram_icon_f)?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<?php }
						if ($pinterest_icon_f) {?>
							<li class="social-pinterest"><a href="<?php echo esc_url($pinterest_icon_f)?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
						<?php }
						if ($rss_icon_f == "on") {?>
							<li class="social-rss"><a href="<?php echo ($rss_icon_f_other != ""?esc_url($rss_icon_f_other):esc_url(bloginfo('rss2_url')));?>" target="_blank"><i class="fa fa-rss"></i></a></li>
						<?php }
					}?>
				</ul>
			</div><!-- End social-ul -->
		</div><!-- End container -->
	</footer><!-- End footer -->
	
</div><!-- End wrap -->

<div class="go-up"><i class="fa fa-chevron-up"></i></div>

<?php wp_footer(); ?>
</body>
</html>